package producer;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class ProducerAvro {

    public static String TOPIC = "cars-position";

    private Properties kafkaProperties;
    private KafkaProducer<GenericRecord, GenericRecord> producer;
    private Schema valueSchema;
    private GenericRecord valueRecord;
    private ProducerRecord<GenericRecord, GenericRecord> record;
    private Connection connection;

    public ProducerAvro(Connection connection) {
        this.connection = connection;
        initProperties();
        record();
    }

    public void initProperties(){
        kafkaProperties = new Properties();
        kafkaProperties.put("bootstrap.servers", "localhost:9092");
        kafkaProperties.put("key.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProperties.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProperties.put("schema.registry.url","http://localhost:8081");
        kafkaProperties.put("acks", "all");
    }

    public void record(){
        producer = new KafkaProducer<>(kafkaProperties);
        valueSchema = SchemaBuilder.record("ValueRecord")
                .fields()
                //.name("time").type().intType().noDefault()
                .name("group_id").type().intType().noDefault()
                .name("car_id").type().stringType().noDefault()
                .name("latitude").type().doubleType().noDefault()
                .name("longitude").type().doubleType().noDefault()
                .name("status").type().booleanType().noDefault()
                .endRecord();

        String query = "SELECT * FROM taxi_driver ORDER BY time";

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                valueRecord = new GenericData.Record(valueSchema);
                //valueRecord.put("time", resultSet.getInt("time"));
                valueRecord.put("group_id", resultSet.getInt("group_id"));
                valueRecord.put("car_id", resultSet.getString("car_id"));
                valueRecord.put("latitude", resultSet.getDouble("latitude"));
                valueRecord.put("longitude", resultSet.getDouble("longitude"));
                valueRecord.put("status", resultSet.getBoolean("status"));
                record = new ProducerRecord<>(TOPIC, null, valueRecord);
                producer.send(record);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
