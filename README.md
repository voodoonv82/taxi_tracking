basic level without visualization


**`TAXI TRACKING`** 
                                    
----------------------------------------------- >>>>>> Kafka <<<<<< ---------------------------------------------

_Installation_
    `https://docs.confluent.io/3.0.0/installation.html`

_Starting confluent_
    `confluent start`

_Creating topic:_
   ` kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic cars-position`
   ` kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic cars-counter`
   ` kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic cars-alerts`
   ` kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic cars-last-car-positions`

_Avro console consumer:_
    `kafka-avro-console-consumer --topic taxi_tracking --bootstrap-server localhost:9092 --from-beginning`
    
_Topics list:_ 
    `kafka-topics --list --zookeeper localhost:2181`


------------------------------------------------- >>>>> Memsql <<<<<< ----------------------------------------------

_Master aggregator:_
   `memsql-ops memsql-deploy -r master -P 3307`

_Leaf:_
    `memsql-ops memsql-deploy -r leaf -P 3308`

_MemSQl commandline:_
    `mysql -u root -h 127.0.0.1 -P 3306 --prompt="memsql> "`

_Raw data storage:_
    `CREATE TABLE taxi_driver(time int, car_id text, group_id int, latitude double, longitude double, status bit, PRIMARY KEY(car_id, group_id));`

_Raw data:_
    `LOAD DATA INFILE '/home/alduin/Downloads/taxi_drive.csv' INTO TABLE taxi_driver FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'(time, car_id, group_id, latitude, longitude, status);`
    
    
--------------------------------------------------- >>>>> Cassandra <<<<< -------------------------------------------

_Cassandra commandline:_
    `cqlsh`
    
_Keyspaces list:_
    `DESCRIBE KEYSPACES`
    
_Create Keyspace:_

`CREATE  KEYSPACE [IF NOT EXISTS] taxi_tracking
   WITH REPLICATION = { 
      'class' : 'SimpleStrategy', 
      'replication_factor' : 3 
   }`
    
_CREATE table:_

`CREATE TABLE [IF NOT EXISTS] taxi_driver(car_id text, group_id int, latitude double, longitude double, status boolean, PRIMARY KEY(group_id, car_id))`



`java -jar /home/alduin/Documents/taxi_tracking/src/main/java/connector/stream-reactor-0.3.0-3.3.0/bin/kafka-connect-cli-1.0.2-all.jar create cassandra-sink-connector < /home/alduin/Documents/taxi_tracking/src/main/java/connector/cassandra-sink-connector.properties
`


https://github.com/Landoop/stream-reactor/releases/tag/0.3.0



https://github.com/ErdtR/kafka-stream-and-connect/blob/master/kafka-streams-example/src/main/java/Main.java
https://github.com/confluentinc/examples/blob/3.3.0-post/kafka-connect-streams/src/main/java/io/confluent/examples/connectandstreams/jdbcgenericavro/StreamsIngest.java



center --- > 49.842547, 24.026544


документація як візуалізувати маркери https://developers.google.com/maps/documentation/javascript/markers?hl=en
серверна частина https://drive.google.com/drive/folders/12F6KcJmVKXqU3EF-aX14SKA8Lvena0jv
запити з jsна сервер https://api.jquery.com/jquery.get/


https://github.com/phamcongsonit/nodejs-google-map-v3/blob/master/index.html

https://github.com/phamcongsonit/nodejs-google-map-v3








AIzaSyBvMSfVxVHBPxFQR_jW6akB_mbsxxXuWGY


https://developers.google.com/maps/documentation/javascript/examples/map-simple?hl=ru




https://drive.google.com/open?id=1AZmZsIKTBp1uklFkjfI10rTl3RkVCVCJ


https://developers.google.com/maps/documentation/javascript/importing_data?hl=ru



https://github.com/fabiorogeriosj/example-node-cassandra/blob/master/www/index.html



https://developers.google.com/maps/documentation/javascript/marker-clustering?hl=ru



http://picua.org/img/2017-12/21/x1p4j9lkxgebfj1kf9dzep2s6.png